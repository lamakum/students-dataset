import numpy as np
def count_inversion(left, right):
    """
    This function use merge sort algorithm to count the number of
    inversions in a permutation of two parts (left, right).
    Parameters
    ----------
    left: ndarray
        The first part of the permutation
    right: ndarray
        The second part of the permutation
    Returns
    -------
    result: ndarray
        The sorted permutation of the two parts
    count: int
        The number of inversions in these two parts
    """
    result = []
    count = 0
    i, j = 0, 0
    left_len = len(left)
    while i < left_len and j < len(right):
        if left[i] <= right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            count += left_len - i
            j += 1
    result += left[i:]
    result += right[j:]

    return result, count

def mergeSort_rec(lst):
    """
    This function count the number of inversions in a permutation by calling
    count_inversion recursively.
    Parameters
    ----------
    lst: ndarray
        The permutation
    Returns
    -------
    result: ndarray
        The sorted permutation
    (a + b + c): int
        The number of inversions
    """
    lst = list(lst)
    if len(lst) <= 1:
        return lst, 0
    middle = int( len(lst) / 2 )
    left, a   = mergeSort_rec(lst[:middle])
    right, b  = mergeSort_rec(lst[middle:])
    result, c = count_inversion(left, right)
    return result, (a + b + c)



def distance(A, B=None):
    """
    This function computes the kendall's-tau distance between two permutations
    using merge sort algorithm.
    If only one permutation is given, the distance will be computed with the
    identity permutation as the second permutation
   Parameters
   ----------
   A: ndarray
        The first permutation
   B: ndarray, optionnal
        The second permutation (default is None)
   Returns
   -------
   int
        The kendall's-tau distance between both permutations
    """
    if B is None : B = list(range(len(A)))

    A = np.asarray(A)
    B = np.asarray(B)
    n = len(A)

    # check if A contains NaNs
    msk = np.isnan(A)
    indexes = np.array(range(n))[msk]

    if indexes.size: # A contains NaNs
        # reverse the indexes
        indexes = sorted(indexes, reverse=True)
        for i in indexes: # delete all NaNs and their associated values in B
            A = np.delete(A, i)
            B = np.delete(B, i)
    # check if B contains NaNs
    msk = np.isnan(B)
    indexes = np.array(range(n - len(indexes)))[msk]

    if indexes.size: # B contains NaNs
        # reverse the indexes
        indexes = sorted(indexes, reverse=True)
        for i in indexes: # delete all NaNs and their associated values in A
            A = np.delete(A, i)
            B = np.delete(B, i)

    inverse = np.argsort(B)
    compose = A[inverse]
    _, distance = mergeSort_rec(compose)
    return distance
